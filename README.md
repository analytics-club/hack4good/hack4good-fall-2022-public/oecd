# OECD

## Overview
This repo contains the analyses and results that our team developed together with OECD for the Hack4Good edition 2022 in conjunction with Analytics Club ETH.

The main part of the code are:

- Tweet extraction from Twitter API: extract tweets and replies to those tweets based on user ids
- Translation: input tweets in original language and return translated tweets (in English)
- Topic modeling: Based on text data (either policy documents or tweets), define topics of the overall corpus and assign probabilities to each document for belonging to each of the categories (topic modeling based on [Latent Dirichlet Allocation](https://de.wikipedia.org/wiki/Latent_Dirichlet_Allocation))
- Agreement/disagreement model: classifier that categorises text into “agreeing”, “disagreeing”, “neutral” categories —> to be used to check to which extent twitter users replying to government agency tweets agree/disagree with the message of the tweet

## Our analysis pipeline

More specifically, we first used topic modeling to assign topics to Science, Technology, Innovation (STI) policy documents we had access to, and then used these same topic models to predict the topics of tweets from selected government ministries.

Next, we sub-selected tweets related to a selected topic (green energy) and further sub-classified these tweets into sub-topics within the green energy domain. Finally, for individual ministries, we analysed the reactions to tweets depending on the topic of green energy they were primarily related to as classified by our topic model.

## Working with our code

The data to reproduce our findings is provided in the data-directory of this repo. When working with these files, you do not have to use the twitter and translation .py-scripts, but you can jump into the jupyter notebooks containing our analyses straight away. To work on your own analyses, you need to first gather tweets via the twitter API using the tweets and translation .py-scripts. Then, you may again look at our notebooks for ideas for how to analyse the collected data.
