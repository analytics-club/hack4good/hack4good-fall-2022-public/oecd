import pandas as pd
import numpy as np

import re
import string
import emoji_data_python

from nltk import word_tokenize, pos_tag
import nltk

from sklearn.feature_extraction.text import CountVectorizer
from gensim import matutils, models

import os
from google.cloud import translate_v2 as translate

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] ='gct_key.json'
client = translate.Client()

class Translator():

  def __init__(self, df):
    self.df = df
    self.df_tweets = []
    self.text_en = []

  def drop_ref_tweets(self, df):
    tweets_post = df.loc[tweets.referenced_tweets.isna(),:]
    tweets_post = df.drop("referenced_tweets", axis=1)
    return tweets_post

  def clean_tweets(self, text):
    text = re.sub(r'\d+', '', text)
    text= re.sub(r"^\s+|\s+$", "",text) 
    text = re.sub(r"http\S+", "", text)
    text = re.sub(r'[^\w\s]','',text)
    text = emoji_data_python.get_emoji_regex().sub("", text)
    text = " ".join(text.split())
    return text

  def translate(self):
    self.df = self.drop_ref_tweets(self.df)

    idx = self.df.index.tolist()[0]
    len = self.df.shape[0]

    for _, row in self.df.iterrows():
      clean_text = self.clean_tweets(row['text'])
      translation = client.translate(clean_text, target_language='en')['translatedText']
      self.text_en.append(translation)
      idx += 1
      print("translating...",idx,"/",len)

  def save(self,filepath):
    self.df_tweets['text_en'] = self.text_en
    self.df_tweets.to_csv('filepath', index=False)
    print("created csv file !")