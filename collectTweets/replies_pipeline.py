import argparse
import os
import sys
import time

import numpy as np
import pandas as pd
import requests
from tqdm import tqdm
from typing import Tuple

from tralslate import Translator

os.environ['TOKEN'] = "AAAAAAAAAAAAAAAAAAAAAKKwewEAAAAAw%2FKTl6ZvG3ivY3UWe9rP%2FXkkTp4%3DrnqeW2aHSlajwTEArRWptB9ziyEaefFGs9ecj7ky3FFQprfF81"

def auth():
    return os.getenv('TOKEN')

def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers

def create_url_tweets(keyword: str, max_results : int = 10, start_time: str = '2006-04-01T00:00:00.000Z',
                      end_time: str = None, next_token = None) -> Tuple[str, dict]:
    # Writes the url needed for the query
    search_url = "https://api.twitter.com/2/tweets/search/all"
    # Values of 'tweets.field', 'user.fields' and 'place.fields' can be changed according to the desdired results
    # check https://developer.twitter.com/ for more informations
    query_params = {'query': keyword,
                    'start_time': start_time,
                    'max_results': max_results,
                    'expansions': 'author_id,in_reply_to_user_id,geo.place_id',
                    'tweet.fields': 'id,text,created_at,lang,public_metrics,referenced_tweets,conversation_id',
                    'user.fields': 'id,name,username,created_at,description,public_metrics,verified',
                    'place.fields': 'full_name,id,country,country_code,geo,name,place_type',
                    'next_token': next_token}
    if end_time is not None: # All tweets untul present date will be scrapped
        query_params['end_time'] = end_time
    return (search_url, query_params)

def get_tweets(url: str, headers:dict, params: dict):
    response = requests.request("GET", url, headers = headers, params = params)
    if response.status_code == 429:
        time.sleep(4) #Sleeping because we have exeded the number of allowed requests
        return get_tweets(url, headers, params)
    if response.status_code != 200: # The were some error in the process
        print("Endpoint Response Code: " + str(response.status_code))
        raise Exception(response.status_code, response.text)
    if 'next_token' in response.json()['meta'].keys(): #Checking if we have come to the bottom of the query
        next_token = response.json()['meta']['next_token']
    else:
        next_token = None
    if 'data' in response.json().keys(): #returning the results if any
        return response.json()['data'], next_token
    else:
        return [], next_token

def clean(df: pd.DataFrame) -> pd.DataFrame:
    # Returns a clean dataframe, we chose to keep ids as strings, because otherwise therewere converted to floats where translating to csv which made a mess.
    likes, retweets, rep, quotes = [], [], [], []

    for _, row in tqdm(df.iterrows()):
        retweets.append(row['public_metrics']['retweet_count'])
        likes.append(row['public_metrics']['like_count'])
        rep.append(row['public_metrics']['reply_count'])
        quotes.append(row['public_metrics']['quote_count'])
    df['retweets'] = retweets
    df['likes'] = likes
    df['quotes'] = quotes
    df['replies'] = rep

    new_ref=[]
    for _, row in tqdm(df.iterrows()):
        if type(row.referenced_tweets)==float:
            new_ref.append(None)
        else:
            new_ref.append((row.referenced_tweets[0]['type'], row.referenced_tweets[0]['id']))
    df['referenced_tweets']=new_ref
    return df.drop(columns=['public_metrics'])

def translate(df: pd.DataFramem, fdest: str) -> None:
    translated_df = Translator(df).translate()
    translated_df.save(fdest)

def scrap(forigin: str, fdest: str) -> None:
    #Takes a filename from which it takes a list of ids of accounts and return the dataframe with the replies
    #We read the file
    tweets = pd.read_csv(forigin)
    bearer_token = auth()
    headers = create_headers(bearer_token)
    n = tweets.id.size
    token = None
    data = []
    #In this iterate by bags of 10 ids, because the more we ask for at a time the quicker it takes, but the string cannot be too long
    #10 appears like the optimal number
    for i in tqdm(range(n//10)):
        #First we build the query (keyword)
        keyword = f'in_reply_to_tweet_id:{int(tweets["id"][10*i])} OR quotes_of_tweet_id:{int(tweets["id"][10*i])}'
        for j in range(10*i+1,10*i+10):
            keyword += f' OR in_reply_to_tweet_id:{int(tweets["id"][j])} OR quotes_of_tweet_id:{int(tweets["id"][j])}'
        #We start scrapping tweets
        url = create_url_tweets(keyword, max_results = 500)
        response_data, token = get_tweets(url[0], headers, url[1])
        data += response_data.copy()
        while token is not None:
            #We continue to scrap until we got all the answers from that query
            url = create_url_tweets(keyword, max_results = 500, next_token = token)
            response_data, token = get_tweets(url[0], headers, url[1])
            data += response_data.copy()
    
    #Same for the last <10 ids
    keyword = f'in_reply_to_tweet_id:{int(tweets["id"][10*(n//10)])} OR quotes_of_tweet_id:{int(tweets["id"][10*(n//10)])}'
    for j in range(10*(n//10),n):
            keyword += f' OR in_reply_to_tweet_id:{int(tweets["id"][j])} OR quotes_of_tweet_id:{int(tweets["id"][j])}'
    url = create_url_tweets(keyword, max_results = 500)
    response_data, token = get_tweets(url[0], headers, url[1])

    data += response_data.copy()
    while token is not None:
        url = create_url_tweets(keyword, max_results = 500, next_token = token)
        response_data, token = get_tweets(url[0], headers, url[1])
        data += response_data.copy()
    
    #We clean the dataframe
    replies = clean(pd.DataFrame(data))

    #Done!
    translate(tweets, fdest)

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description = 'Take tweets ids from forigin, scraps their replies and writes them in fdest.')
    ap.add_argument('forigin', metavar='FILE', type=str, nargs=1,
                    help="The file from we which to take the tweets' ids")
    ap.add_argument('fdest', metavar='FILE', type=str, nargs=1,
                    help="The file in which to write the replies")
    opts = ap.parse_args(sys.argv[1:])
    forigin = opts.forigin[0] #file from which we take the tweets ids
    fdest = opts.fdest[0] #files in which we write the replies data

    scrap(forigin, fdest) 
    sys.exit("All replies successfully scrapped!")
