These files are a snapshot view of an ongoing annotation project. They were produced in connection with the paper "Unshared task: (Dis)agreement in online debates", presented on the "Unshared Task at the ACL 2016 Workshop on Argument Mining" (2013, Association for Computational Linguistics, Stroudsburg, PA). For more information about the workshop, see http://argmining2016.arg.tech/index.php/home/call-for-papers/unshared-task/.

The paper "Unshared task: (Dis)agreement in online debates", by Maria Skeppstedt, Magnus Sahlgren, Carita Paradis and Andreas Kerren, describes the text used and the annotation task. 

The two txt-files
—————————
Format for the two txt-files ("disputed_top_1000.txt" and "other_top_1000.txt"):
agreement/disagreement: d = disagreement, a = agreement
contrast: c = the sentence contains two opinions that are contrasted
tact/rudeness: t = the sentence includes politeness, r = the sentence includes rudeness
could be either: c = It cannot be determined without context whether it is disagreement or agreement that is expressed
both: b = both agreement and disagreement are expressed in the sentence

The two txt-files are not part of the data provided for the shared task, but are downloaded from createdebate.com


The unshared task data
—————————
The files in the folder "argumentationMiningUnsharedtaskVariantA" were those provided as one of the data sets for the unshared task. They have been annotated with Brat, and the annotations are provided in the format resulting from annotations with this tool.